![BBC Store](https://store.bbc.com/sites/all/themes/barcelona/images/Logo-BBC.png)

# BBC Worldwide - OLC Delta Queue


This library provides with the common queue message and serializer required for OLC delta updates.

## Installation

Preferred installation method is via composer. Since we're not published in Packagist, you'll need a little extra
config in your composer.json.

First, add an entry to the `repositories` key in composer.json:
```json
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://bitbucket.org/bbcworldwide/php-queues.git"
    }
    {
      "type": "vcs",
      "url":  "https://bitbucket.org/bbcworldwide/olc-delta-queue.git"
    }
  ],

```

Second, run `composer require bbcworldwide/olc-delta-queue`.

That's it.

Example:

```json
{
  "name": "some/project",
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://bitbucket.org/bbcworldwide/php-queues.git"
    }
    {
      "type": "vcs",
      "url":  "https://bitbucket.org/bbcworldwide/olc-delta-queue.git"
    }
  ],
  "require":      {
    "php": ">=5.6.0",
    "bbcworldwide/olc-delta-queue": "^1.0"
  }
}
```

## How to

### Queue client

You'll need to instantiate the [Serializer](src/Serializer.php) as well as the queue client. The easiest way is to 
use the provided factories, like so (example for SQS):

```php
<?php
use BBCWorldwide\Queue\Client\SQS\Factory as SQSFactory;
use BBCWorldwide\OLC\Queue\SerializerFactory;

$queue = SQSFactory::getInstance(
    SerializerFactory::getInstance(),
    $psrLogger,  // Optional
    $queueName,  // Optional, you can use $queue->subscribe instead
    $awsRegion   // Defaults to eu-west-1,
    $awsKey,     // Optional
    $awsSecret,  // Optional
    $awsEndpoint // Optional
);
```

This will instantiate all the dependencies for the queue client (eg SQS from AWS SDK), dealing with stuff like 
IAM-roled nodes that don't require AWS keys.

### Making messages

There's also a factory you can use to create messages out of XML files which will also ensure they're properly sanitised
and will ensure the correct type and IDs are hydrated into the message:

```php
<?php
use BBCWorldwide\OLC\Queue\MessageFactory;

$message = MessageFactory::getInstance($xml);
```
