<?php

namespace BBCWorldwide\OLC\Queue\Tests;

use BBCWorldwide\OLC\Queue\Message;
use BBCWorldwide\OLC\Queue\MessageFactory;
use BBCWorldwide\OLC\Queue\SerializerFactory;
use BBCWorldwide\OLC\Queue\Xml\Parser;
use BBCWorldwide\Queue\Message\MessageInterface;
use PHPUnit\Framework\TestCase;

class FunctionalTest extends TestCase
{
    /**
     * @var string
     */
    protected $episodeXmlRaw;

    /**
     * @var string
     */
    protected $episodeXmlSanitised;

    /**
     * @var string
     */
    protected $assetXml;

    public function setUp()
    {
        parent::setUp();

        $this->episodeXmlRaw       = file_get_contents(__DIR__ . '/fixtures/EpisodeRaw.xml');
        $this->episodeXmlSanitised = trim(file_get_contents(__DIR__ . '/fixtures/EpisodeSanitised.xml'));
        $this->assetXml = trim(file_get_contents(__DIR__ . '/fixtures/Asset.xml'));
    }

    /**
     * @test
     * @expectedException \BBCWorldwide\OLC\Queue\Xml\Exception\InvalidXmlException
     */
    public function parserChokesOnDodgyXml()
    {
        new Parser('foo');
    }

    /**
     * @test
     */
    public function messageAcceptsDatetimeAndString()
    {
        $dateString = '2017-06-06T16:03:15+00:00';
        $dateTime   = new \DateTime($dateString);

        $message = new Message();
        $message->setReceivedAt($dateString);
        self::assertEquals($dateTime, $message->getReceivedAt());

        $message->setReceivedAt($dateTime);
        self::assertSame($dateTime, $message->getReceivedAt());
    }

    /**
     * @test
     */
    public function endToEndEpisodeSerializeTest()
    {
        $serializer = SerializerFactory::getInstance();

        $receivedAt    = '2017-06-06T16:03:15+00:00';
        $correlationId = uniqid('foo', true);

        // Original XML with dodgy windows encoding
        $message = MessageFactory::getInstance($this->episodeXmlRaw, $correlationId);
        $message->setReceivedAt($receivedAt);

        $serialized = $serializer->serialize($message);

        $expected = [
            'entityId'        => 'EP00006643',
            'entityType'      => 'Episode',
            'xml'             => $this->episodeXmlSanitised,
            'receivedAt'      => $receivedAt,
            'hash'            => md5($this->episodeXmlSanitised),
            'forcedOverwrite' => false,
            'correlationId'   => $correlationId,
            'messageId'       => null,
            'allMetadata'     => [],
        ];

        self::assertJsonStringEqualsJsonString(json_encode($expected), $serialized);
    }

    /**
     * @test
     */
    public function endToEndAssetSerializeTest()
    {
        $serializer = SerializerFactory::getInstance();

        $receivedAt    = '2017-06-06T16:03:15+00:00';
        $correlationId = uniqid('foo', true);

        // Original XML with dodgy windows encoding
        $message = MessageFactory::getInstance($this->assetXml, $correlationId);
        $message->setReceivedAt($receivedAt);

        $serialized = $serializer->serialize($message);

        $expected = [
            'entityId'        => '63d0162b-3005-4f39-b861-60318053f45c',
            'entityType'      => 'AssetFulfillment',
            'xml'             => $this->assetXml,
            'receivedAt'      => $receivedAt,
            'hash'            => md5($this->assetXml),
            'forcedOverwrite' => false,
            'correlationId'   => $correlationId,
            'messageId'       => null,
            'allMetadata'     => [],
        ];

        self::assertJsonStringEqualsJsonString(json_encode($expected), $serialized);
    }

    /**
     * @test
     */
    public function endToEndUnserializeTest()
    {
        $serializer = SerializerFactory::getInstance();

        $receivedAt = '2017-06-06T16:03:15+00:00';
        $serialized = json_encode([
            'entityId'        => 'EP00006643',
            'entityType'      => 'Episode',
            'xml'             => $this->episodeXmlSanitised,
            'receivedAt'      => $receivedAt,
            'hash'            => md5($this->episodeXmlSanitised),
            'forcedOverwrite' => false,
            'messageId'       => null,
            'allMetadata'     => [],
        ]);

        $expected = new Message();
        $expected
            ->setReceivedAt($receivedAt)
            ->setEntityId('EP00006643')
            ->setEntityType('Episode')
            ->setXml($this->episodeXmlSanitised);

        self::assertEquals($expected, $serializer->deserialize($serialized));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Message must be an instance
     */
    public function serializerDislikesUnsupportedMessages()
    {
        $message = $this->getMockBuilder(MessageInterface::class)->getMock();

        SerializerFactory::getInstance()->serialize($message);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage XML
     */
    public function messageDoesntValidateWithoutXml()
    {
        $message = new Message();
        $message
            ->setEntityType('fo')
            ->setEntityId('bar');

        $message->selfValidate();
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Entity Id
     */
    public function messageDoesntValidateWithoutEntityId()
    {
        $message = new Message();
        $message
            ->setEntityType('fo')
            ->setXml('bar');

        $message->selfValidate();
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Entity Type
     */
    public function messageDoesntValidateWithoutEntityType()
    {
        $message = new Message();
        $message
            ->setEntityId('fo')
            ->setXml('bar');

        $message->selfValidate();
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Correlation ID
     */
    public function messageDoesntValidateWithoutCorrelationId()
    {
        $message = new Message();
        $message
            ->setEntityId('fo')
            ->setXml('bar')
            ->setEntityType('foobar');

        $message->selfValidate();
    }

    /**
     * @test
     */
    public function messageValidates()
    {
        $message = new Message();
        $message
            ->setEntityType('bar')
            ->setEntityId('fo')
            ->setCorrelationId('foobar')
            ->setXml('bar');

        self::assertNull($message->selfValidate());
    }

    /**
     * @test
     */
    public function summaryHasTheExpectedShape()
    {
        $receivedAt    = '2017-06-06T16:03:15+00:00';
        $correlationId = uniqid('foo', true);

        $id   = 'iddd';
        $type = 'typee';
        $xml  = '<xml />';

        $message = new Message();
        $message
            ->setEntityType($type)
            ->setEntityId($id)
            ->setXml($xml)
            ->setCorrelationId($correlationId)
            ->setReceivedAt($receivedAt);

        $expected = [
            'xml'               => $xml,
            'hash'              => md5($xml),
            'entityId'          => $id,
            'entityType'        => $type,
            'receivedAt'        => '2017-06-06T16:03:15+00:00',
            'correlationId'     => $correlationId,
            'isForcedOverwrite' => false,

        ];

        self::assertEquals($expected, $message->summary());
    }
}
