<?php

namespace BBCWorldwide\OLC\Queue\Xml;

use duncan3dc\Bom\Util;

/**
 * This class is a preprocessor for Biztalk XML messages before they go into validation, in order to remove known
 * issues on them:
 *   - Files are microsoft-y UTF-16LE >> convert to UTF-8 and remove BOM.
 *   - Some known cases of mismatch in between the given schemas and the actual XML.
 *
 * @author BBC Worldwide
 */
class Sanitiser
{
    /**
     * @var string
     */
    private $xml;

    /**
     * Given an XML file from Biztalk, sanitise it up.
     *
     * @param string $xml
     *
     * @return string
     */
    public function sanitise(string $xml): string
    {
        $this->xml = $xml;

        return $this
            ->removeBom()
            ->correctUtf()
            ->trim()
            ->done();
    }

    /**
     * Removes BOM off a string.
     *
     * @return self
     */
    private function removeBom(): self
    {
        $this->xml = Util::removeBom($this->xml);

        return $this;
    }

    /**
     * Change XML declaration for text encoding to UTF-8 down from UTF-16, as removeBom does this conversion.
     *
     * @return self
     */
    private function correctUtf(): self
    {
        $this->xml = str_replace('utf-16', 'utf-8', $this->xml);

        return $this;
    }

    /**
     * Remove any extra spaces/tabs/newlines at either end of the string.
     *
     * @return self
     */
    private function trim(): self
    {
        $this->xml = trim($this->xml);

        return $this;
    }

    /**
     * Returns the result of the whole process.
     *
     * @return string
     */
    private function done(): string
    {
        return $this->xml;
    }
}
