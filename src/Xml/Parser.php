<?php

namespace BBCWorldwide\OLC\Queue\Xml;

/**
 * Biztalk XML entity parser.
 *
 * @author BBC Worldwide
 */
class Parser
{
    /**
     * @var \SimpleXMLElement
     */
    private $element;

    /**
     * EntityParser constructor.
     * @param string $xml
     *
     * @throws Exception\InvalidXmlException
     */
    public function __construct(string $xml)
    {
        try {
            libxml_use_internal_errors();
            $this->element = new \SimpleXMLElement($xml);
        } catch (\Exception $ex) {
            throw new Exception\InvalidXmlException($ex->getMessage());
        } finally {
            libxml_clear_errors();
        }
    }

    /**
     * Get entity ID from the Code or AssetIdentifier element.
     *
     * @return string
     */
    public function getId(): string
    {
        if (isset($this->element->Code)) {
            return (string) $this->element->Code;
        }
        if (isset($this->element->VideoAssetDetails->AssetIdentifier)) {
            return (string) $this->element->VideoAssetDetails->AssetIdentifier;
        }
        throw new Exception\InvalidXmlException('No entity ID found');
    }

    /**
     * Get entity type from the root element name.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->element->getName();
    }
}
