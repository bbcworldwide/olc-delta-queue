<?php

namespace BBCWorldwide\OLC\Queue;

use BBCWorldwide\OLC\Queue\Xml\Parser;
use BBCWorldwide\OLC\Queue\Xml\Sanitiser;

/**
 * Queue message factory.
 *
 * @author BBC Worldwide
 */
class MessageFactory
{
    /**
     * Generates a queue message given the XML string. It will also do some cleanup on the XML itself.
     *
     * @param string $xmlInput
     * @param string $correlationId
     *
     * @return \BBCWorldwide\OLC\Queue\Message
     */
    public static function getInstance(string $xmlInput, string $correlationId): Message
    {
        $sanitiser = new Sanitiser();
        $xml       = $sanitiser->sanitise($xmlInput);

        $parser = new Parser($xml);
        $id     = $parser->getId();
        $type   = $parser->getType();

        $message = new Message();
        $message
            ->setXml($xml)
            ->setEntityId($id)
            ->setEntityType($type)
            ->setCorrelationId($correlationId);

        return $message;
    }
}
