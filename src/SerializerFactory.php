<?php
namespace BBCWorldwide\OLC\Queue;

use BBCWorldwide\Queue\Message\SerializerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;

/**
 * Makes a message serializer pre-configured with default values.
 *
 * @author BBC Worldwide
 * @codeCoverageIgnore
 */
class SerializerFactory
{
    /**
     * Returns a fully configured message serializer.
     *
     * @return SerializerInterface
     * @throws \InvalidArgumentException
     */
    public static function getInstance()
    {
        // Convert datetime to string
        $callback = function ($dateTime) {
            return $dateTime instanceof \DateTime
                ? $dateTime->format(\DateTime::ATOM)
                : '';
        };

        $normalizer = new ObjectNormalizer();
        $normalizer->setCallbacks([
            'receivedAt' => $callback
        ]);

        $symfonySerializer = new SymfonySerializer([$normalizer], [new JsonEncoder()]);

        return new Serializer($symfonySerializer);
    }
}
